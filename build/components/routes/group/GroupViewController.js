

GroupRouteController.resolve = {
	item: function($stateParams, FluroContent) {
        return FluroContent.resource('get/' + $stateParams.id).get({}).$promise;
    },
    stripe:function(FluroContent) {
        return FluroContent.resource('get/59dc77b256249c3b12f52ac4').get({}).$promise;

    },
    freeForm: function($stateParams, FluroContent) {
        return FluroContent.resource('get/59dc0a3fdc790f107b0e79ab').get({}).$promise;
    },
    paidForm: function($stateParams, FluroContent) {
        return FluroContent.resource('get/59ddc0d95d06c3362e311e02').get({}).$promise;
    },
    seo: function(FluroSEOService, Asset, item) {

        //Set the page title as the title of the article
        FluroSEOService.pageTitle = item.title;

        //Get variables from your article item
        var articleImage = _.get(item, 'data.publicData.image');
        var articleDescription = _.get(item, 'data.publicData.shortDescription');

        FluroSEOService.imageURL = Asset.imageUrl(articleImage, 640);
        FluroSEOService.description = articleDescription;
        return true;
    },
}

/////////////////////////////////////////////////////

// app.controller('JoinController', function($scope) {

// 	console.log('JOIN CONTROLLEr')
	
// });

/////////////////////////////////////////////////////

function GroupRouteController($scope, $q, item, freeForm, stripe, paidForm, $timeout, FluroContent) {

	$scope.item = item;
	$scope.freeForm = freeForm;
	$scope.paidForm = paidForm;
	$scope.stripe = stripe;


	/////////////////////////////////////////////////////

	$scope.config = {}

	

	function preselect() {
		_.set($scope.config, 'free.model.connectGroup', item._id);
		_.set($scope.config, 'paid.model.connectGroup', item._id);
	}

	$timeout(preselect, 1000);

	/////////////////////////////////////////////////////

	$scope.formSubmitted = function(response) {

		//Now Join
		console.log('Form Submitted', response);
		
		
		var contactRequests = _.map(response.contacts, function(contactID) {
			if(contactID._id) {
				contactID = contactID._id;
			}

			//Join the contact to fluro
			var request = FluroContent.endpoint('teams/' + item._id + '/join').save({
				_id:contactID
			}).$promise;

			return request;

		})
		$q.all(contactRequests).then(function(res) {
			console.log('Join Attempt', res)
			$scope.item.submitted = true;
		}, function(err) {
			console.log('Join Attempt', err)
			$scope.item.submitted = true;
		});
		
	}

	console.log('ALLL READY')

	/////////////////////////////////////////////////////

	$scope.leaderRow = _.find(item.assignments, function(entry) {
		return String(entry.title).toLowerCase() == 'leader';
	});

	/////////////////////////////////////////////////////

	//Using https://github.com/devmark/angular-slick-carousel 
    // $scope.sliderConfig = {
    //     enabled: true,
    //     // draggable: true,
    //     // dots: true,
    //     // arrows: true,
    //     // speed:600,
    //     // autoplay: true,
    //     // autoplaySpeed: 4000,
    //     // method: {},
    //     // slidesToShow: 5,
    //     // slidesToScroll: 1,
    //     // infinite: false,
    //     // // event: {
    //     // //     beforeChange: function(event, slick, currentSlide, nextSlide) {
    //     // //         console.log('Event', event)
    //     // //     },
    //     // //     afterChange: function(event, slick, currentSlide, nextSlide) {
    //     // //         console.log('Event', event)
    //     // //     }
    //     // // },
    //     // responsive: [{
    //     //         breakpoint: 768,
    //     //         settings: {
    //     //             slidesToShow: 2,
    //     //             slidesToScroll: 2
    //     //         }
    //     //     }, {
    //     //         breakpoint: 480,
    //     //         settings: {
    //     //             slidesToShow: 1,
    //     //             slidesToScroll: 1
    //     //         }
    //     //     }
    //     //     // You can unslick at a given breakpoint now by adding:
    //     //     // settings: "unslick"
    //     //     // instead of a settings object
    //     // ]
    // };



}

