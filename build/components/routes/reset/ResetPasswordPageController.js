app.controller('ResetPasswordPageController', function($scope, $rootScope, PurchaseService, $state, FluroTokenService, $http, user, token) {
    
    
    $scope.settings = {};

    $scope.persona = user;
    $scope.token = token;

    //////////////////////////////////////


   


    //////////////////////////////////////
    //////////////////////////////////////
    //////////////////////////////////////
    //////////////////////////////////////
    //////////////////////////////////////


    $scope.update = function() {
        $scope.settings.state = 'processing';
        
        
        function resetSuccess(res) {
            $scope.settings.state = 'complete';
            $state.go('home')
        }
        
        function resetFailed(err) {
            $scope.settings.state = 'error';
            $scope.settings.errorMessage = err.data;
        }
        
        var request = FluroTokenService.updateUserWithToken(token, $scope.persona, {application:true});

        request.then(resetSuccess, resetFailed);

    }

    //////////////////////////////////////

/**

    //Load the URL
    var url = '/fluro/application/reset/' + $stateParams.token;
    
    //////////////////////////////////////
    
    function success(res) {
        
        $scope.persona = res.data;
    }
    
    function failed(err) {
        console.log('Error', err);
    }
    
    //////////////////////////////////////

    $http.get(url).then(success, failed);

    

    /**/
    
})