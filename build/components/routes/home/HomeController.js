

HomeRouteController.resolve = {
	seo: function(FluroSEOService, Asset, $rootScope) {
        FluroSEOService.pageTitle = null;

        return true;
    },
    groups:function(FluroContent) {
        return FluroContent.endpoint('content/_query/59ddc26c5d06c3362e311e3f').query().$promise;
    }
}

function HomeRouteController($scope, groups, $filter) {


    ///////////////////////////////////////////

	$scope.groups = _.map(groups, function(group) {

        group.leaders = _.find(group.assignments, function(assignment) {

            var title = assignment.title.toLowerCase();
            return (title == 'leader' || title == 'leaders');
        });

        console.log('mapped', group);

        return group;
    });

    ///////////////////////////////////////////

    $scope.availableTimes = _.chain(groups)
    .map('data.cgMeetingDay')
    .compact()
    .uniq()
    .value()

    $scope.availableSuburbs = _.chain(groups)
    .map('data.cgLocation.suburb')
    .compact()
    .uniq()
    .value()

    $scope.availableTypes = _.chain(groups)
    .map('data.cgNet')
    .compact()
    .uniq()
    .value()

    ///////////////////////////////////////////



    $scope.search = {};


    $scope.$watch('search', updateSearch,true);

    ///////////////////////////////////////////

    $scope.resetSearch = function() {
        $scope.search = {};
    }

    ///////////////////////////////////////////

    function updateSearch() {

        var filtered = $scope.groups;

        var search = $scope.search;
        var terms = search.terms;

        if(terms && terms.length) {
            filtered = $filter('filter')(filtered, terms);
        }

        if(search.suburb) {
            filtered = _.filter(filtered, function(group){
                var match = _.get(group, 'data.cgLocation.suburb');

                return match == search.suburb;
            });
        }

        if(search.time) {
            filtered = _.filter(filtered, function(group){
                var match = _.get(group, 'data.cgMeetingDay');

                return match == search.time;
            });
        }

        if(search.type) {
            filtered = _.filter(filtered, function(group){
                var match = _.get(group, 'data.cgNet');

                return match == search.type;
            });
        }


        $scope.filteredItems = filtered;
    }


}

