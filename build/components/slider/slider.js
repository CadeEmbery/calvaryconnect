app.directive('slider', function() {
    return {
        restrict: 'E',
        transclude:true,
        replace:true,
        templateUrl:'slider/slider.html',
    }
});