app.factory('dateutils', function() {

    ////console.log('date.utils')
    // validate if entered values are a real date
    function validateDate(date) {


        // store as a UTC date as we do not want changes with timezones
        var d = new Date(Date.UTC(date.year, date.month, date.day));
        var result = d && (d.getMonth() === date.month && d.getDate() === Number(date.day));

        // ////console.log('Valid date', result, date);
        return result;
    }

    // reduce the day count if not a valid date (e.g. 30 february)
    function changeDate(date) {
        if (date.day > 28) {
            date.day--;
            return date;
        }
        // this case should not exist with a restricted input
        // if a month larger than 11 is entered
        else if (date.month > 11) {
            date.day = 31;
            date.month--;
            return date;
        }
    }

    var self = this;
    this.days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
    this.months = [{
        value: 0,
        name: 'January'
    }, {
        value: 1,
        name: 'February'
    }, {
        value: 2,
        name: 'March'
    }, {
        value: 3,
        name: 'April'
    }, {
        value: 4,
        name: 'May'
    }, {
        value: 5,
        name: 'June'
    }, {
        value: 6,
        name: 'July'
    }, {
        value: 7,
        name: 'August'
    }, {
        value: 8,
        name: 'September'
    }, {
        value: 9,
        name: 'October'
    }, {
        value: 10,
        name: 'November'
    }, {
        value: 11,
        name: 'December'
    }];

    return {
        checkDate: function(date) {

            if (!date.day) {
                return false;
            }

            if (date.month === 0) {

            } else {
                if (!date.month) {
                    return false;
                }
            }

            if (!date.year) {
                return false;
            }

            if (validateDate(date)) {
                // update the model when the date is correct
                return date;
            } else {
                // change the date on the scope and try again if invalid
                return this.checkDate(changeDate(date));
            }
        },
        get: function(name) {
            return self[name];
        }
    };
});

app.directive('dobSelect', ['dateutils',
    function(dateutils) {
        return {
            restrict: 'E',
            replace: true,
            require: 'ngModel',
            scope: {
                model: '=ngModel',
                hideAge: '=?',
                hideDates: '=?',
            },
            controller: ['$scope', 'dateutils',
                function($scope, dateutils) {


                    ////console.log('dob.select')


                    // set up arrays of values
                    $scope.days = dateutils.get('days');

                    $scope.months = dateutils.get('months');

                    /////////////////////////////////////////////////////

                    if (!$scope.model) {
                        //$scope.model = new Date();
                       // ////console.log('No way man')
                    } else {

                        //////console.log('TESTING', $scope.model, _.isDate($scope.model))
                        if (!_.isDate($scope.model)) {
                            $scope.model = new Date($scope.model);
                        }
                    }

                    /////////////////////////////////////////////////////

                    // split the current date into sections
                    var dateFields = {};

                    if ($scope.model) {
                        //Update the date fields
                        dateFields.day = new Date($scope.model).getDate();
                        dateFields.month = new Date($scope.model).getMonth();
                        dateFields.year = new Date($scope.model).getFullYear();

                        $scope.dateFields = dateFields;


                        /////////////////////////////////////////////////////

                        //Age
                        $scope.age = moment().diff($scope.model, 'years');

                    }

                    /////////////////////////////////////////////////////

                    // validate that the date selected is accurate
                    $scope.checkDate = function() {
                        // update the date or return false if not all date fields entered.
                        var date = dateutils.checkDate($scope.dateFields);
                        if (date) {
                            $scope.dateFields = date;
                        }
                    };

                    /////////////////////////////////////////////////////

                    function dateFieldsChanged(newDate, oldDate) {

                        //////console.log('Date fields Changed', newDate);
                        if (newDate == oldDate) {
                            return;
                        }

                        //Stop Watching Age
                        stopWatchingAge();


                        /////////////////////////////////

                        if (!$scope.dateFields) {
                            return ////console.log('No datefields object');
                        }

                        if (!$scope.dateFields.year) {
                            return ////console.log('No Year');
                        }

                        /////////////////////////////////

                        if ($scope.dateFields.month === 0) {

                        } else {
                            if (!$scope.dateFields.month) {
                                return ////console.log('No Month');
                            }
                        }

                        /////////////////////////////////

                        if (!$scope.dateFields.day) {
                            return ////console.log('No day');
                        }

                        /////////////////////////////////

                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        date.setHours(0, 0, 0, 0);

                        if (!$scope.model) {
                            $scope.model = new Date();
                        }


                        //Set the models details
                        $scope.model.setDate(date.getDate());
                        $scope.model.setMonth(date.getMonth());
                        $scope.model.setFullYear(date.getFullYear());

                        //Get the difference
                        var diff = moment().diff($scope.model, 'years');

                        //Update the age
                        $scope.age = parseInt(diff);


                        //Start Watching Age
                        startWatchingAge();
                    }


                    //Age was changed
                    function ageChanged(newAge, oldAge) {

                        if (newAge == oldAge) {
                            return;
                        }
                        ////console.log('Age changed', newAge, oldAge);

                        var today = new Date();
                        today.setHours(0, 0, 0, 0);

                        var currentYear = today.getFullYear();
                        var diff = parseInt(currentYear) - parseInt(newAge);

                        if (diff) {

                            //Stop Watching the Date
                            stopWatchingDate();

                            if (!$scope.dateFields) {

                                // if(!$scope.model) {
                                //     $scope.model = new Date();
                                // }

                                var dateFields = {};
                                //Update the date fields
                                //dateFields.day = new Date($scope.model).getDate();
                                //dateFields.month = new Date($scope.model).getMonth();
                                dateFields.year = diff; //new Date($scope.model).getFullYear();
                                $scope.dateFields = dateFields;
                            } else {
                                $scope.dateFields.year = diff;
                            }

                            //Set the year
                            // if ($scope.dateFields.year != diff) {
                            //     $scope.dateFields.year = diff;
                            // }

                            //Start Watching the date again
                            startWatchingDate();


                        } else {
                            ////console.log('NOPE', newAge, currentYear, diff);
                        }


                    }

                    /////////////////////////////////////////////////////

                    var ageWatcher;

                    function startWatchingAge() {
                        ageWatcher = $scope.$watch('age', ageChanged);
                    }

                    function stopWatchingAge() {
                        if (ageWatcher) {
                            ageWatcher();
                        }
                    }

                    /////////////////////////////////////////////////////

                    var dateWatcher;

                    function startWatchingDate() {
                        dateWatcher = $scope.$watch('dateFields', dateFieldsChanged, true);
                    }

                    function stopWatchingDate() {
                        if (dateWatcher) {
                            dateWatcher();
                        }
                    }

                    /////////////////////////////////////////////////////


                    //Start watching for changes to either
                    startWatchingAge();
                    startWatchingDate();
                    /*
                    /////////////////////////////////////////////////////

                    $scope.updateAge = function() {
                        ////console.log('Change Age');
                        var today = new Date();
                       
                        var currentYear = today.getFullYear();
                        var diff = currentYear - age;
                        ////console.log('AGE', age, diff, currentYear);

                        //Set the year
                        $scope.dateFields.year = diff
                    }

                    /////////////////////////////////////////////////////

                    $scope.updateDate = function() {
                        ////console.log('Change Date');
                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        //Set the year
                        $scope.model.setDate(date.getDate());
                    }

                    /////////////////////////////////////////////////////

                    $scope.updateMonth = function() {
                        ////console.log('Change Month');
                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        //Set the year
                        $scope.model.setMonth(date.getMonth());
                    }

                    /////////////////////////////////////////////////////

                    $scope.updateYear = function() {
                        ////console.log('Change Year');
                        var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);
                        //Set the year
                        $scope.model.setFullYear(date.getFullYear());
                    }

                    /////////////////////////////////////////////////////

                    $scope.$watch('model', function(newModel, oldModel) {


                        ////console.log('Model Change');
                        $scope.dateFields.day = new Date(newModel).getUTCDate();
                        $scope.dateFields.month = new Date(newModel).getUTCMonth();
                        $scope.dateFields.year = new Date(newModel).getUTCFullYear();

                    })
*/

                    /*
                $scope.$watch('dateFields', function() {

                    //Update our date fields
                    $scope.dateFields.day = new Date($scope.model).getUTCDate();
                    $scope.dateFields.month = new Date($scope.model).getUTCMonth();
                    $scope.dateFields.year = new Date($scope.model).getUTCFullYear();

                    //Get the actual Date
                    $scope.model.
                    //var date = new Date($scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day);

                    //////console.log('tESTING', $scope.dateFields.year, $scope.dateFields.month, $scope.dateFields.day, date);
                    $scope.age = moment().diff(date, 'years');
                    $scope.model = date;

                }, true);

                $scope.updateAge = function() {
                    if (newAge != oldAge) {

                        var today = new Date();
                        var age = Number(newAge);
                        var currentYear = today.getFullYear();
                        var diff = currentYear - age;
                        ////console.log('AGE', age, diff, currentYear);
                        //Set the year
                        $scope.dateFields.year = diff
                    }

                });
*/




                }
            ],
            templateUrl: 'fluro-dob-select/fluro-dob-select.html',
            link: function(scope, element, attrs, ctrl) {
                if (attrs.yearText) {
                    scope.yearText = true;
                }
                // allow overwriting of the
                if (attrs.dayDivClass) {
                    angular.element(element[0].children[0]).removeClass('form-group col-xs-3');
                    angular.element(element[0].children[0]).addClass(attrs.dayDivClass);
                }
                if (attrs.dayClass) {
                    angular.element(element[0].children[0].children[0]).removeClass('form-control');
                    angular.element(element[0].children[0].children[0]).addClass(attrs.dayClass);
                }
                if (attrs.monthDivClass) {
                    angular.element(element[0].children[1]).removeClass('form-group col-xs-5');
                    angular.element(element[0].children[1]).addClass(attrs.monthDivClass);
                }
                if (attrs.monthClass) {
                    angular.element(element[0].children[1].children[0]).removeClass('form-control');
                    angular.element(element[0].children[1].children[0]).addClass(attrs.monthClass);
                }
                if (attrs.yearDivClass) {
                    angular.element(element[0].children[2]).removeClass('form-group col-xs-4');
                    angular.element(element[0].children[2]).addClass(attrs.yearDivClass);
                }
                if (attrs.yearClass) {
                    angular.element(element[0].children[2].children[0]).removeClass('form-control');
                    angular.element(element[0].children[2].children[0]).addClass(attrs.yearClass);
                }

                // set the years drop down from attributes or defaults
                var currentYear = parseInt(attrs.startingYear, 10) || new Date().getFullYear();
                var numYears = parseInt(attrs.numYears, 10) || 100;
                var oldestYear = currentYear - numYears;

                scope.years = [];
                for (var i = currentYear; i >= oldestYear; i--) {
                    scope.years.push(i);
                }



                // pass down the ng-disabled property
                scope.$parent.$watch(attrs.ngDisabled, function(newVal) {
                    scope.disableFields = newVal;
                });

                /*
      // ensure that fields are entered
      var required = attrs.required.split(' ');
      ctrl.$parsers.push(function(value) {
        angular.forEach(required, function (elem) {
          if(!angular.isNumber(elem)) {
            ctrl.$setValidity('required', false);
          }
        });
        ctrl.$setValidity('required', true);
      });
/**/
                // var validator = function(){
                //   var valid = true;
                //   // all fields entered
                //   angular.forEach(required, function (elem) {
                //     if(!angular.isNumber(elem)) {
                //       valid = false;
                //     }
                //   });
                //   // if(isNaN(scope.dateFields.day) && isNaN(scope.dateFields.month) && isNaN(scope.dateFields.year)){
                //   //   valid = true;
                //   // }
                //   // else if(!isNaN(scope.dateFields.day) && !isNaN(scope.dateFields.month) && !isNaN(scope.dateFields.year)){
                //   //   valid = true;
                //   // }
                //   // else valid = false;

                //   ctrl.$setValidity('rsmdatedropdowns', valid);
                // };

                // scope.$watch('dateFields.day', function(){
                //   validator();
                // });

                // scope.$watch('dateFields.month', function(){
                //   validator();
                // });

                // scope.$watch('dateFields.year', function(){
                //   validator();
                // });
            }
        };
    }
]);



/*
app.directive('dobSelect', function($document) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'fluro-dob-select/fluro-dob-select.html',
        scope: {
            model: '=ngModel',
        },
        link: function($scope, element, attr) {

            /////////////////////////////////////

            $scope.dateObject = {};

            /////////////////////////////////////

            $scope.settings = {}

            /////////////////////////////////////

            function elementClick(event) {
                //Clicked inside
                event.stopPropagation();
            }

            function documentClick(event) {
                //Clicked outside
                $scope.$apply(function() {
                    $scope.open = false;
                });
            }

            /////////////////////////////////////

            if (!$scope.model) {
                $scope.model = new Date();
            }

            /////////////////////////////////////

            $scope.$watch('model', function(data) {
                if ($scope.model) {
                    var date = new Date($scope.model);
                    $scope.dateObject.year = date.getFullYear();
                    $scope.dateObject.month = date.getMonth();
                    $scope.dateObject.day = date.getDay();
                }
            })

            /////////////////////////////////////

            $scope.$watch('dateObject', function(newValue, oldValue) {
                if (newValue != oldValue) {
                    var data = newValue;
                    $scope.model = new Date(data.year, data.month, data.day);
                }

                ////console.log($scope.model);
            }, true);

            /*
            /////////////////////////////////////

            $scope.$watch('model', function(newValue, oldValue) {
                if(newValue != oldValue) {
                   
                   ////console.log($scope.model.getYear(), $scope.model.getMonth(), $scope.model.getDate());

                   $scope.dateObject.year = Number($scope.model.getYear());
                   $scope.dateObject.month = Number($scope.model.getMonth());
                   $scope.dateObject.day = Number($scope.model.getDay());
                   
                }

            }, true);


            /////////////////////////////////////
            /*
            $scope.$watch('model', function() {
                if (!$scope.model) {
                    $scope.model = new Date();
                }

                $scope.dateObject.year = $scope.model.getYear();
                $scope.dateObject.month = $scope.model.getYear();
                $scope.dateObject.day = $scope.model.getYear();
            })



            //Listen for when this date select is open
            $scope.$watch('settings.open', function(bol) {
                if (bol) {
                    element.on('click', elementClick);
                    $document.on('click', documentClick);
                } else {
                    element.off('click', elementClick);
                    $document.off('click', documentClick);
                }
            })

        },
        controller: function($scope) {






        }

    };

});
/**/