var app = angular.module('fluro', [
    'fluro.boilerplate',
    'ngMap',
]);


app.config(function($stateProvider) {

    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////

     $stateProvider.state('home', {
        url: '/',
        templateUrl: 'routes/home/home.html',
        resolve: HomeRouteController.resolve,
        controller:HomeRouteController,
    });

     $stateProvider.state('group', {
        url: '/group/:id',
        templateUrl: 'routes/group/group.html',
        resolve: GroupRouteController.resolve,
        controller:GroupRouteController,
    });


     $stateProvider.state('details', {
        url: '/details/:id',
        templateUrl: 'routes/details/details.html',
        resolve: DetailsRouteController.resolve,
        controller:DetailsRouteController,
    });

     $stateProvider.state('detail', {
        url: '/detail/:id',
        templateUrl: 'routes/details/details.html',
        resolve: DetailsRouteController.resolve,
        controller:DetailsRouteController,
    });


      $stateProvider.state('faq', {
        url: '/faq',
        templateUrl: 'routes/faq/faq.html',
        resolve: FAQRouteController.resolve,
        controller:FAQRouteController,
    });

    ///////////////////////////////////////////

    $stateProvider.state('search', {
        url: '/search',
        templateUrl: 'routes/search/search.html',
        controller: 'SearchPageController',
        data: {
            requireLogin: true,
        },
        resolve: {
            seo: function(FluroSEOService) {
                FluroSEOService.pageTitle = 'Search';
                return true;
            },
            articles: function(FluroContent) {
                return FluroContent.resource('article').query().$promise;
            },
        }
    });

    ///////////////////////////////////////////

    $stateProvider.state('myaccount', {
        url: '/my-account',
        templateUrl: 'routes/myaccount/myaccount.html',
        controller: 'MyAccountController',
        data: {
            requireLogin: true,
        },
        resolve: {
            seo: function(FluroSEOService, $rootScope) {
                FluroSEOService.pageTitle = $rootScope.user.firstName;
                return true;
            },
            purchases: function(FluroContent) {
                return FluroContent.endpoint('my/purchases', false, true).query({
                    populateLicenses: true,
                }).$promise;
            },
            posts: function(FluroContentRetrieval, $rootScope) {

                if (!$rootScope.user || !$rootScope.user.persona) {
                    return [];
                }

                //////////////////////////////////////////////

                var queryDetails = {
                    _type: "post",
                    definition: {
                        $in: ['comment', 'note'],
                    },
                    managedAuthor: $rootScope.user.persona,
                }

                var selectFields = 'title parent definition created';

                /////////////////////////////////////////

                //Request an update of all available events today
                var promise = FluroContentRetrieval.query(queryDetails, null, null, {
                    noCache: true,
                    select: selectFields
                }, null);

                return promise;
            },
            paymentMethods: function($q, FluroContent, PurchaseService) {

                //Find all payment methods for the user

                //Defer the promise
                var deferred = $q.defer();

                //Check the application data for the payment gateway to use
                var paymentGateway = PurchaseService.applicationPaymentGateway();

                //If a gateway is set
                if (paymentGateway) {
                    //Retrieve all cards for the current user for the specified payment gateway
                    return PurchaseService.retrievePaymentMethods(paymentGateway._id);
                } else {
                    deferred.resolve([]);
                }

                //Return the promise;
                return deferred.promise;
            },
        }
    });

    ///////////////////////////////////////////

    $stateProvider.state('login', {
        url: '/login?returnTo&package',
        abstract: true,
        template: '<div ui-view></div>',
        controller: 'LoginPageController',
    });

    $stateProvider.state('login.form', {
        url: '',
        templateUrl: 'routes/login/login.html',
        data: {
            denyAuthenticated: true,
        },
        resolve: {
            seo: function(FluroSEOService) {
                FluroSEOService.pageTitle = 'Sign in';
                return true;
            },
        }

    });

    $stateProvider.state('login.forgot', {
        url: '/forgot',
        templateUrl: 'routes/login.forgot/forgot.html',
        data: {
            denyAuthenticated: true,
        },
        resolve: {
            seo: function(FluroSEOService) {
                FluroSEOService.pageTitle = 'Forgot Password';
                return true;
            },
        }
    });


    ///////////////////////////////////////////

    $stateProvider.state('signup', {
        url: '/signup?returnTo',
        templateUrl: 'routes/signup/signup.html',
        controller: 'SignupPageController',
        data: {
            denyAuthenticated: true,
        },
        resolve: {
            seo: function(FluroSEOService) {
                FluroSEOService.pageTitle = 'Sign up';
                return true;
            },
        }
    });

    ///////////////////////////////////////////

    $stateProvider.state('reset', {
        url: '/reset-password?token',
        templateUrl: 'routes/reset/reset.html',
        controller: 'ResetPasswordPageController',
        resolve: {
            token: function($stateParams) {
                return $stateParams.token;
            },
            user: function($q, token, FluroTokenService) {
                //Deferred
                var deferred = $q.defer();

                // var token = $stateParams.token;

                var request = FluroTokenService.retrieveUserFromResetToken(token, {
                    application: true
                });

                request.then(function(res) {
                    return deferred.resolve(res.data);
                }, deferred.reject)

                return deferred.promise;

            },
            seo: function(FluroSEOService) {
                FluroSEOService.pageTitle = 'Reset Password';
                return true;
            },
        }
    });

    ///////////////////////////////////////////

})